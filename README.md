This is a small script that will allow users to quickly and easily enable autologin for their system, for specified users.

Use cases include kiosks, digital signage, lazy people, or people who have encrypted drives and don't need Windows security.


Feel free to implement any part of this script in your own, or use it in your business



TO USE:
1. Open the *.ps1 file with a text editor, or PowerShell ISE
2. Replace ExampleUsername with the username of the account you wish to autologin to
3. Replace ExamplePassword with the password of the account you wish to autologin to
4. Once the script is complete, simply restart your computer and you should be greeted by your desktop