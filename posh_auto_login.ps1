# Edit in between the little quotes
$username = 'ExampleUsername'
$password = 'ExamplePassword'

# Don't edit below this unless you know what you're doing :)
$registryPath = 'HKLM:\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon'

# Check for Administrator
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Write-Warning "You are not running as an Administrator. Please re-run this script as an administrator!"
    Break
}

# Do basic data verification
If($username -eq $null, '', 'ExampleUsername'){
    Write-Host('Username field cannot be blank! Open script in text editor and edit $username field')
    exit
}
If($password -eq $null, '', 'ExamplePassword'){
    Write-Host('Password field cannot be blank! Open script in text editor and edit $password field')
    exit
}

 
Write-Host("Setting up registry for autologin")
 
$key = Get-Item -LiteralPath $registryPath
 
# Setup DefaultUsername value
If($key.GetValue('DefaultUsername') -eq $null){
    Write-Host("Creating DefaultUsername")
    New-ItemProperty -LiteralPath $registryPath -Name 'DefaultUsername' -Value $username
}
Else{
    Write-Host("Editing DefaultUsername")
    Set-ItemProperty -Path $registryPath -Name 'DefaultUsername' -Value $username
}
 
# Setup DefaultPassword value
If($key.GetValue('DefaultUsername') -eq $null){
    Write-Host("Creating DefaultPassword")
    New-ItemProperty -LiteralPath $registryPath -Name 'DefaultPassword' -Value $pasword
}
Else{
    Write-Host("Editing DefaultPassword")
    Set-ItemProperty -Path $registryPath -Name 'DefaultPassword' -Value $password
}
 
# Verify successful change
If($key.GetValue('DefaultUsername') -eq $username){
    Write-Host("DefaultUsername change successfull")
}
Else{
    Write-Error("DefaultUsername change FAILED!")
}
 
If($key.GetValue('DefaultPassword') -eq $password){
    Write-Host("DefaultPassword change successfull")
}
Else{
    Write-Error("DefaultPassword change FAILED!")
}